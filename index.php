<?php

session_start();
if (empty($_SESSION['csrf_token'])) {
    $_SESSION['csrf_token'] = bin2hex(random_bytes(32));
}
require_once __DIR__ . "/app/Helper/functions.php";
require_once __DIR__ . "/vendor/autoload.php";

$app = new App\Core\Application([
    "DB_HOST" => "127.0.0.1",
    "DB_NAME" => "cities_countries",
    "DB_USERNAME" => "root",
    "DB_PASSWORD" => "root",
]);

$app->Router->get("/", "App\Controllers\Auth\AuthController@LoginGet");
$app->Router->get("/login", "App\Controllers\Auth\AuthController@LoginGet");
$app->Router->get("/admin/logout", "App\Controllers\Auth\LogoutController@Logout");
$app->Router->post("/login/auth", "App\Controllers\Auth\AuthController@LoginPost");

$app->Router->get("/admin/city", "App\Controllers\Admin\CityController@Index");
$app->Router->get("/admin/city/create", "App\Controllers\Admin\CityController@Create");
$app->Router->get("/admin/city/edit", "App\Controllers\Admin\CityController@Edit");
$app->Router->post("/admin/city/update", "App\Controllers\Admin\CityController@Update");
$app->Router->post("/admin/city", "App\Controllers\Admin\CityController@Store");

$app->Router->get("/admin/country", "App\Controllers\Admin\CountryController@Index");
$app->Router->get("/admin/country/create", "App\Controllers\Admin\CountryController@Create");
$app->Router->get("/admin/country/show", "App\Controllers\Admin\CityController@Show");
$app->Router->get("/admin/country/edit", "App\Controllers\Admin\CountryController@Edit");
$app->Router->post("/admin/country/update", "App\Controllers\Admin\CountryController@Update");
$app->Router->post("/admin/country", "App\Controllers\Admin\CountryController@Store");

$app->Router->get("/login", "App\Controllers\Auth\AuthController@LoginGet");
$app->Router->post("/login/auth", "App\Controllers\Auth\AuthController@LoginPost");

//debug($app);
$app->run();