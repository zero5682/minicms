#Installing & running server

1) Download project from git
2) Run "composer install" command in project root
3) Run server "php -S localhost:8080"

#Adding Database

4) Create MySql database 
5) Add database host, database, username, password to Core/Application.php
6) Add database migration from sql folder

#Login

User: admin
Password: minicms
