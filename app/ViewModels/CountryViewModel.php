<?php  

namespace App\ViewModels;

use App\Models\Country;

class CountryViewModel
{
    protected $Id;
    protected $Name;

    public function __construct(Country $countries) 
    {
        $this->Id = $countries->GetId();
        $this->Name = $countries->GetName();
    }

    public function GetId() 
    {
        return $this->Id;
    }

    public function GetName() 
    {
        return $this->Name;
    }
}