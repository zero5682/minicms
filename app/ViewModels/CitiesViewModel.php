<?php  

namespace App\ViewModels;

use App\Models\Model;

class CitiesViewModel
{
    protected $Cities;

    public function __construct(array $cities) 
    {
        $this->Cities = $cities;
    }

    public function GetCities() 
    {
        return $this->Cities;
    }
}