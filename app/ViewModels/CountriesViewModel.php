<?php  

namespace App\ViewModels;

use App\Models\Model;

class CountriesViewModel
{
    protected $Countries;

    public function __construct(array $countries) 
    {
        $this->Countries = $countries;
    }

    public function GetCountries() 
    {
        return $this->Countries;
    }
}