<?php  

namespace App\Models;

use App\Models\Model;

use \PDO;

class City extends Model
{
    public function __construct($id = 0, $name = "") 
    {
        parent::__construct();
        $this->Id = $id;
        $this->Name = $name;
        /*
        echo "<pre>";
        var_dump($this);
        echo "</pre>";
        */
    }

    public function GetName() : string
    {
        return $this->Name;
    }

    public function GetId() : int
    {
        return $this->Id;
    }

    public function GetCountryId() : int
    {
        return $this->CountryId;
    }

    public function SetId($id) : City
    {
        $this->Id = $id;
        return $this;
    }

    public function SetCountryId($id) : City
    {
        $this->CountryId = $id;
        return $this;
    }

    public function SetName($name) 
    {
        $this->Name = $name;
        return $this;
    }

    public function Save() : bool
    {
        try
        {
            $stmt = $this->PDO->prepare("INSERT INTO cities (name, country_id) VALUES (?, ?)");
            $stmt->execute([$this->Name, $this->CountryId]);
            $stmt = null;
            return true;
        }
        catch(Exception $ex) 
        {
            return false;
        }
    }

    public function GetAll() : array
    {
        $stmt = $this->PDO->prepare("SELECT id, name FROM cities");
        $stmt->execute();
        $arr = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $data = [];
        if(!$arr) return $data;
        $stmt = null;

        $data = [];

        foreach($arr as $city) 
        {
            if ($city["name"] == "") continue;
            $data[] = new $this($city["id"], $city["name"]);
        }

        return $data;
    }

    public function GetById(int $id) : City
    {
        $stmt = $this->PDO->prepare("SELECT id, name FROM cities WHERE id = ?");
        $stmt->execute([$id]);
        $arr = $stmt->fetchAll(PDO::FETCH_ASSOC);

        $this->SetId($arr[0]["id"]);
        $this->SetName($arr[0]["name"]);

        $arr = null;

        return $this;
    }

    public function GetByCountryId(int $id) : array
    {
        $stmt = $this->PDO->prepare("SELECT id, name FROM cities WHERE country_id = ?");
        $stmt->execute([$id]);
        $arr = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $data = [];
        if(!$arr) return $data;
        $stmt = null;

        foreach($arr as $city) 
        {
            if ($city["name"] == "") continue;
            $data[] = new $this($city["id"], $city["name"]);
        }

        return $data;
    }

    public function Update($name, $id) : bool
    {
        try 
        {
            $sql = "UPDATE cities SET name = ? WHERE id = ?";
            $stmt= $this->PDO->prepare($sql);
            $stmt->execute([$name, $id]);
            return true;
        } 
        catch(Exception $ex) 
        {
            return false;
        }
    }
}