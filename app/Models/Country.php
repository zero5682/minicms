<?php  

namespace App\Models;

use App\Models\Model;

use \PDO;
use \PDOException;

class Country extends Model
{
    public function __construct($id = 0, $name = "") 
    {
        parent::__construct();
        $this->Id = $id;
        $this->Name = $name;
        /*
        echo "<pre>";
        var_dump($this);
        echo "</pre>";
        */
    }

    public function GetName() 
    {
        return $this->Name;
    }

    public function GetId() 
    {
        return $this->Id;
    }

    public function SetId($id) 
    {
        $this->Id = $id;
        return $this;
    }

    public function SetName($name) 
    {
        $this->Name = $name;
        return $this;
    }

    public function Save() 
    {
        try 
        {
            $stmt = $this->PDO->prepare("INSERT INTO countries (name) VALUES (?)");
            $stmt->execute([$this->Name]);
            $stmt = null;
            return true;
        } 
        catch(Exception $ex) 
        {
            return false;
        }
    }

    public function GetAll() 
    {
        $data = [];

        try 
        {
            $stmt = $this->PDO->prepare("SELECT * FROM countries");
            $stmt->execute();
            $arr = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(!$arr) exit('No rows');
            $stmt = null;
    
            foreach($arr as $city) 
            {
                if ($city["name"] == "") continue;
                $data[] = new $this($city["id"], $city["name"]);
            }
    
            return $data;
        } 
        catch(Exception $ex) 
        {
            return $data;
        }
    }

    public function GetById(int $id) 
    {
        try 
        {   
            $stmt = $this->PDO->prepare("SELECT * FROM countries WHERE id = ?");
            $stmt->execute([$id]);
            $arr = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(!$arr) exit('No rows');
            $stmt = null;

            $this->SetId($arr[0]["id"]);
            $this->SetName($arr[0]["name"]);
            return $this;
        }
        catch(Exception $ex) 
        {
            return $this;
        }
    }

    public function GetCountryCities() 
    {
        $data = [];

        try 
        {
            $stmt = $this->PDO->prepare("SELECT * FROM countries ");
            $stmt->execute();
            $arr = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(!$arr) exit('No rows');
            $stmt = null;
    
            foreach($arr as $city) 
            {
                if ($city["name"] == "") continue;
                $data[] = new $this($city["id"], $city["name"]);
            }
    
            return $data;
        } 
        catch(Exception $ex) 
        {
            return $data;
        }
    }

    public function Update($name, $id) : bool
    {
        try 
        {
            $sql = "UPDATE countries SET name = ? WHERE id = ?";
            $stmt= $this->PDO->prepare($sql);
            $stmt->execute([$name, $id]);
            return true;
        } 
        catch(Exception $ex) 
        {
            return false;
        }
    }
}