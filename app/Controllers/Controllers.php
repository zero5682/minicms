<?php

namespace App\Controllers;

use App\Core\View;

class Controllers 
{
    public function View($view, $data = null) 
    {
        return (new View($view, $data))->render();
    }

    public function isloggedin() 
    {
        if (empty($_SESSION["auth-login"])) return false;
        return (bool)$_SESSION["auth-login"] == true;
    }
}