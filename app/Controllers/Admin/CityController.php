<?php

namespace App\Controllers\Admin;

use App\Core\Request;
use App\Controllers\Controllers;

use App\Models\City;
use App\Models\Country;
use App\ViewModels\CitiesViewModel;
use App\ViewModels\CountryViewModel;
use App\Core\SessionManager;

class CityController extends Controllers
{

    public function __construct() 
    {
        if (!$this->IsLoggedIn()) 
        {
            header("location: /login");
            exit;
        }
    }

    public function Index() 
    {
        return $this->View("admin.city.index", [
            "cities" => new CitiesViewModel((new City())->GetAll())
        ]);
    }

    public function Show(Request $request) 
    {
        $cities = (new City())->GetByCountryId($request->GetQuery());
        $currentCountry = (new Country())->GetById($request->GetQuery());

        return $this->View("admin.city.index", [
            "cities" => new CitiesViewModel($cities),
            "country" => new CountryViewModel($currentCountry),
            "country_id" => $request->GetQuery()
        ]);
    }

    public function Create(Request $request) 
    {
        return $this->View("admin.city.create", [
            "country_id" => $request->GetQuery()
        ]);
    }

    public function Store(Request $request) 
    {
        $country_id = $request->getBody()["country_id"];

        $city = new City();
        $city->SetName($request->getBody()["name"]);
        $city->SetCountryId($country_id);
        $city->Save();

        SessionManager::SetFlash("Edukalt lisatud!", 1);

        header("Location: /admin/country/show?id=$country_id");
        exit;
    }

    public function Edit(Request $request) 
    {
        $city = (new City())->GetById($request->GetQuery()); 

        return $this->View("admin.city.edit", [
            "city" => $city
        ]);
    }

    public function Update(Request $request) 
    {
        $city = (new City())->Update(
            $request->GetBody()["name"],
            $request->GetQuery()
        );

        if (!$city) throw new Exception("Linna muutmisel tekkis viga");

        SessionManager::SetFlash("Edukalt muudetud!", 1);

        header("location: /admin/city/edit?id=" . $request->GetQuery());
        exit;
    }
}