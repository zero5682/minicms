<?php

namespace App\Controllers\Admin;

use App\Core\Request;
use App\Models\Country;
use App\Controllers\Controllers;
use App\ViewModels\CountryViewModel;
use App\ViewModels\CountriesViewModel;
use App\Core\SessionManager;

class CountryController extends Controllers
{
    public function __construct() 
    {
        if (!$this->IsLoggedIn()) 
        {
            header("location: /login");
            exit;
        }
    }

    public function Index() 
    {
        return $this->View("admin.country.index", [
            "countries" => new CountriesViewModel((new Country())->GetAll())
        ]);
    }

    public function Create() 
    {
        return $this->View("admin.country.create");
    }

    public function Store(Request $request) 
    {
        $city = new Country();
        $city->SetName($request->getBody()["name"]);
        if (!$city->Save()) throw new Exception("Lisamisel tekkis viga");

        SessionManager::SetFlash("Edukalt lisatud!", 1);

        header('Location: /admin/country');
        exit;
    }

    public function Edit(Request $request) 
    {
        $country = (new Country())->GetById($request->GetQuery());

        return $this->View("admin.country.edit", [
            "country" => new CountryViewModel($country)
        ]);
    }

    public function Update(Request $request) 
    {
        $country = (new Country())->Update(
            $request->GetBody()["name"],
            $request->GetQuery()
        );

        if (!$country) throw new Exception("Riigi muutmisel tekkis viga");

        SessionManager::SetFlash("Edukalt muudetud!", 1);

        header("location: /admin/country");
        exit;
    }
}