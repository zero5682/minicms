<?php

namespace App\Controllers\Auth;

use App\Models\User;
use App\Core\Request;
use App\Controllers\Controllers;
use App\Core\SessionManager;

class AuthController extends Controllers 
{
    public function __construct() 
    {
        if ($this->IsLoggedIn()) 
        {
            header("location: /admin/country");
            exit;
        }
    }

    public function LoginGet() 
    {
        return $this->View("auth.login");
    }

    public function LoginPost(Request $request) 
    {
        $user = (new User())->Mock(
            $request->getBody()["login"],
            $request->getBody()["password"]
        );

        debug($user);

        if ($user) 
        {
            $_SESSION["auth-login"] = 1;
            header("location: /admin/country");
            exit;
        }
        SessionManager::SetFlash("Sisselogimisel tekkis viga!", 1);
        header("location: /login");
        exit;
    }

    public function Logout() 
    {
        //session_destroy();
        $_SESSION["auth-login"] = 0;
        //unset($_SESSION["auth-login"]);
        unset($_SESSION["flash"]);
        header("Location: /login");
    }
}