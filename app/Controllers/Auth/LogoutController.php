<?php

namespace App\Controllers\Auth;

use App\Controllers\Controllers;
use App\Core\SessionManager;

class LogoutController extends Controllers 
{

    public function Logout() 
    {
        session_destroy();
        header("Location: /login");
        exit;
    }
}