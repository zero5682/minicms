<h2>Riik -> Loo</h2>

<form method="post" action="/admin/country">
    <div class="form-group row">
        <label for="countryInputName" class="col-sm-1 col-form-label">Riik</label>
        <div class="col-sm-11">
            <input id="countryInputName" type="text" class="form-control form-control-sm" name="name" />
        </div>
    </div>
    <div class="form-group">
        <input type="submit" class="btn btn-primary btn-sm submit-btn" value="Loo">
    </div>
    <?php echo $this->getCsrfField(); ?>
</form>