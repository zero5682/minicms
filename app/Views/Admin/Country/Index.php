<div id="cities-header-title" class="row">
    <div class="cities-header-title-content">
        <h2>Riigid</h2>
    </div>
    <div class="cities-header-title-content">
        <a href="/admin/country/create" class="btn btn-primary btn-sm align-right">+Lisa riik</a>
    </div>
</div>
<?php if ($this->HasFlash()): ?>
        <div class="alert alert-success" role="alert">
            <?php echo $this->GetFlash(); ?>
        </div>
    <?php endif; ?>
<table class="table">
    <thead>
        <th>#</th>
        <th>Riik</th>
        <th></th>
        <th></th>
    </thead>
    <tbody>
        <?php foreach($this->countries->GetCountries() as $key => $country): ?>
        <tr>
            <td class="col-md-2">
                <?php echo $key + 1; ?>
            </td>
            <td class="col-md-3">
                <?php echo $country->GetName(); ?>
            </td>
            <td class="col-md-2">
                <a class="btn btn-success btn-sm btn-align-right" href="/admin/country/show?id=<?php echo $country->GetId(); ?>">Naita</a>
            </td>
            <td class="col-md-2">
                <a class="btn btn-primary btn-sm" href="/admin/country/edit?id=<?php echo $country->GetId(); ?>">Muuda</a>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>