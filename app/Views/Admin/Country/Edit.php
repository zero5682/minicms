<h2>Riik -> Muuda</h2>

<form method="post" action="/admin/country/update?id=<?php echo $this->country->GetId(); ?>">
    <?php if ($this->HasFlash()): ?>
        <div class="alert alert-success" role="alert">
            <?php echo $this->GetFlash(); ?>
        </div>
    <?php endif; ?>

    <div class="form-group row">
        <label for="countryInputName" class="col-sm-1 col-form-label">Riik</label>
        <div class="col-sm-11">
            <input 
                id="countryInputName" 
                type="text" 
                class="form-control form-control-sm" 
                name="name" 
                placeholder="Nimi" 
                value="<?php echo $this->country->GetName(); ?>" 
            />
        </div>
    </div>
    <div class="form-group">
        <input type="submit" class="btn btn-primary btn-sm submit-btn" value="Muuda">
    </div>
    <?php echo $this->getCsrfField(); ?>
</form>