<h2>Linn -> Loo</h2>

<form method="post" action="/admin/city">
    <div class="form-group row">
        <label for="cityInputName" class="col-sm-1 col-form-label">Linn</label>
        <div class="col-sm-11">
            <input id="cityInputName" type="text" class="form-control form-control-sm" name="name" />
        </div>
    </div>
    <div class="form-group">
        <input type="submit" class="btn btn-primary btn-sm submit-btn" value="Loo">
    </div>
    <?php echo $this->getCsrfField(); ?>
    <input type="hidden" name="country_id" value="<?php echo $this->country_id; ?>">
</form>