<div id="cities-header-title">
    <div class="cities-header-title-content">
        <h2><?php echo $this->country->GetName() ?></h2>
    </div>
    <div class="cities-header-title-content">
        <a href="/admin/city/create?id=<?php echo $this->country_id; ?>" class="btn btn-primary btn-sm align-right">+Lisa linn</a>   
    </div>
</div>
<?php if ($this->HasFlash()): ?>
        <div class="alert alert-success" role="alert">
            <?php echo $this->GetFlash(); ?>
        </div>
    <?php endif; ?>
<table class="table">
    <thead>
        <th>#</th>
        <th>Linn</th>
        <th></th>
    </thead>
    <tbody>
        <?php foreach($this->cities->GetCities() as $key => $city): ?>
        <tr>
            <td>
                <?php echo $key + 1; ?>
            </td>
            <td>
                <?php echo $city->GetName(); ?>
            </td>
            <td class="col-md-2">
                <a class="btn btn-primary btn-sm" href="/admin/city/edit?id=<?php echo $city->GetId(); ?>">Muuda</a>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>