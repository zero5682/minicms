<h2>Linn -> Muuda</h2>

<form method="post" action="/admin/city/update?id=<?php echo $this->city->GetId(); ?>">
    <?php if ($this->HasFlash()): ?>
        <div class="alert alert-success" role="alert">
            <?php echo $this->GetFlash(); ?>
        </div>
    <?php endif; ?>
    <div class="form-group row">
        <label for="cityInputName" class="col-sm-1 col-form-label">Linn</label>
        <div class="col-sm-11">
            <input 
                id="cityInputName" 
                type="text" 
                class="form-control form-control-sm" 
                name="name" 
                placeholder="Nimi"
                value="<?php echo $this->city->GetName(); ?>" 
            />
        </div>
    </div>
    <div class="form-group">
        <input type="submit" class="btn btn-primary btn-sm submit-btn" value="Muuda">
    </div>
    <?php echo $this->getCsrfField(); ?>
</form>