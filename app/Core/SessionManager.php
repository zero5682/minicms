<?php

namespace App\Core;

class SessionManager 
{
    public static function Set($key, $value) 
    {
        $_SESSION[$key] = $value;
    }

    public static function SetFlash($value, $tries) 
    {
        $_SESSION["flash"]["message"] = $value;
        $_SESSION["flash"]["tries"] = $tries;
    }

    public static function GetFlash() : string
    {
        $flash = self::Get("flash");
        $tries = $flash["tries"] > 0 ? $flash["tries"] - 1 : $flash["tries"];
        
        $tries < 1 ? self::DestroyFalsh("flash") : self::SetFlash("flash", $flash["message"], $tries);

        return ((object)$flash)->message;
    }

    public static function Get($key)
    {
        if (!isset($_SESSION[$key]) && empty($_SESSION[$key])) return "";

        return !empty($_SESSION[$key]) && $_SESSION[$key] == null ? "" : $_SESSION[$key];
    }

    public static function DestroyFalsh($key) 
    {
        $_SESSION[$key] = null;
    }

    public static function HasFlash() 
    {
        return self::Get("flash") != null;
    }
}