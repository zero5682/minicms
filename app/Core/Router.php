<?php

namespace App\Core;

use App\Core\Request;

class Router 
{
    private Request $_request;
    private array $_routes = [
        "get" => [],
        "post" => [],
        "put" => [],
        "post" => [],
    ];

    public function __construct(Request $request) 
    {
        $this->_request = $request;
    }

    public function resolve() 
    {
        $path = $this->_request->GetPath();
        $method = $this->_request->GetMethod();
        $callback = $this->_routes[$method][$path] ?? false;

        if (!$callback) 
        {
            return "Error 404: Lehte ei leitud!";
        }
        else if (is_string($callback)) 
        {
            return $this->render($callback);
        }

        return call_user_func($callback);
    }

    public function render($callback) 
    {
        $layoutsDir = __DIR__ . "/../Views/Layouts";

        require_once "$layoutsDir/header.php";
        if (strpos($callback, ".")) 
        {
            $callback = str_replace(".", "/", $callback);
            require_once __DIR__ . "/../Views/$callback.php";
        } 
        else 
        {
            $method = explode("@", $callback);
            $callback = new $method[0]();
            $callback->{$method[1]}($this->_request);
        }
        
        require_once "$layoutsDir/footer.php";
    }

    public function get($path, $callback) 
    {
        $this->_routes["get"][$path] = $callback;
    }

    public function post($path, $callback) 
    {
        $this->_routes["post"][$path] = $callback;
    }

}