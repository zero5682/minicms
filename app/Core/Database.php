<?php 

namespace App\Core;

use \PDO;
use \PDOException;
use \Exception;

class Database 
{

    public static ?PDO $PDO = null;

    public static function Init(array $config) 
    {
        $dsn = "mysql:host=" . $config["DB_HOST"] . ";dbname=" . $config["DB_NAME"] . ";charset=utf8";
        $options = [
            PDO::ATTR_EMULATE_PREPARES   => false, // turn off emulation mode for "real" prepared statements
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, //turn on errors in the form of exceptions
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, //make the default fetch be an associative array
        ];
        try 
        {
            self::$PDO = new PDO($dsn, $config["DB_USERNAME"], $config["DB_PASSWORD"], $options);
        } 
        catch (Exception $e) 
        {
            echo $e->getMessage();
            exit('Admebaasiga ei saadud ühendust'); //something a user can understand
        }
    }

    public static function GetPdo() 
    {
        return self::$PDO;
    }
}