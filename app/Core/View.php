<?php 

namespace App\Core;

use App\Core\SessionManager;

class View 
{
    private $data;
    private $view;
    private $csrf;

    public function __construct($view, $data = null) 
    {
        $this->csrf = $_SESSION['csrf_token'];
        $this->view = str_replace(".", "/", $view);

        if (is_array($data)) 
        {
            foreach($data as $key => $dd) 
            {
                $this->{$key} = $dd;
            }
        }
    }

    public function render() 
    {
        require_once __DIR__ . "/../Views/$this->view.php";
        return $this; 
    }

    public function getCsrfField() 
    {
        return "<input type='hidden' value='" . $this->csrf . "' name='csrf_token' />";
    }

    public function HasFlash() 
    {
        return SessionManager::HasFlash();
    }

    public function GetFlash() 
    {
        return SessionManager::GetFlash();
    }
}