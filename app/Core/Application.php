<?php

namespace App\Core;

use App\Core\Router;
use App\Core\Request;
use App\Core\Session;
use App\Core\Database;

class Application 
{
    public Router $Router;

    public function __construct(array $dbConfig) 
    {
        $this->Router = new Router(new Request());
        Database::Init($dbConfig);
    }

    public function run() 
    {
        echo $this->Router->resolve();
    }
}