<?php

namespace App\Core;

class Request 
{

    public function GetCurrentUrl() 
    {

        return $_SERVER["REQUEST_URI"];
    }

    public function GetQuery() 
    {
        $query = explode("?", $this->GetCurrentUrl());
        return explode("=", $query[1])[1];
    }

    public function GetPath() 
    {
        $path = $this->GetCurrentUrl() ?? "/";
        $position = strpos($path, "?");
        return $position ? substr($path, 0, $position) : $path;
    }

    public function GetMethod() 
    {
        return strtolower($_SERVER["REQUEST_METHOD"]);
    }

    public function GetBody() 
    {
        $body = [];

        if ($this->GetMethod() === "get") 
        {
            foreach($_GET as $key => $value) 
            {
                if ($value == null) continue;
                $body[$key] = filter_input(INPUT_GET, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        }
        else if ($this->GetMethod() === "post" && $_SESSION["csrf_token"] === $_POST["csrf_token"]) 
        {
            foreach($_POST as $key => $value) 
            {
                if ($value == null) continue;
                $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        }

        return $body;
    }
}